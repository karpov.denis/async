// const cbDone = Symbol('cbDone');
// const cbFail = Symbol('cbFail');
const registerDoneCb = [];
const registerFailCb = [];
class DoAsync {
  constructor(func) {
    // const done = () => null;
    // const fail = () => null;
    // this[cbDone] = [];
    // this[cbFail] = [];
    func(done, fail);
    }

    after(cb) {
      registerDoneCb.push(cb)
      return this
    }

    error(cb) {
      registerFailCb.push(cb)
      return this
    }
  };
  let funcCall = false;

  function done(arg) {
    if(funcCall) {
      return
    }
    funcCall = true;
    setTimeout(() => {
      let result = null;
      registerDoneCb.forEach(item => {
        if(!result) {
          result = item(arg)
        }
      })
    }, 0);
  }

  function fail(arg) {
    if(funcCall) {
      return
    }
    funcCall = true;
    setTimeout(() => {
      let result  = null;
      registerFailCb.forEach(item => {
        if(!result) {
          result = item(arg)
        }
      }) 
    }, 0);
  }

const users = [{ name: 'Jack' }, { name: 'Masha' }];

const getUser = new DoAsync(function(done, fail) {
  console.log(1);
  done(users);
  console.log(2);
  fail('Some Error');
  console.log(3);
  done(null);
})

console.log(4);

getUser.after(function(res) {
  console.log(8, res);
});

console.log(5);

getUser.error(function(err) {
  console.log('it has never been called!', err);
});

console.log(6);

getUser.after(function(res) {
  console.log(9, res);
});

console.log(7);
